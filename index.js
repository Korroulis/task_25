
const { Sequelize, QueryTypes } = require('sequelize');

if (process.env.NODE_ENV !== 'production') {
    require('dotenv').config();
}

const sequelize = new Sequelize ({
    database: process.env.DB_DATABASE,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    host: process.env.DB_HOST,
    dialect: process.env.DB_DIALECT
});

async function countries() {
    try {
        await sequelize.authenticate();
        console.log('Connection established successfully');
        const countries = await sequelize.query('SELECT country.Name FROM country', { type: QueryTypes.SELECT });
        console.log(countries.map(countries => {
            return countries.Name
          }));
    }
    catch (e) {
        console.error(e);
    }
}

async function cities() {
    try {
        const cities = await sequelize.query('SELECT city.Name FROM city', { type: QueryTypes.SELECT });
        console.log(cities.map(cities => {
            return cities.Name
          }));
    }
    catch (e) {
        console.error(e);
    }
}

async function languages() {
    try {
        const languages = await sequelize.query('SELECT DISTINCT Language FROM countrylanguage', { type: QueryTypes.SELECT });
        console.log(languages.map(languages => {
            return languages.Language
          }));
          await sequelize.close();
    }
    catch (e) {
        console.error(e);
    }
}

countries();
cities();
languages();
