#Task 25

1. Create a basic Node app
2. Install Sequelize and dotenv
3. Connect to the World database (Sample Data from # MySQL Installation) using Sequelize
4. Use process.env for database credentials
5. Write 3 functions that use Sequelize Raw Queries to:
- Get all the countries
- Get all the cities
- Get all the languages